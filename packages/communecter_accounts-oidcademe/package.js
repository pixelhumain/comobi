Package.describe({
  summary: 'OpenID Connect (OIDC) ademe for Meteor accounts',
  version: '1.1.0',
  name: 'communecter:accounts-oidcademe',
});

Package.onUse(function (api) {
  api.use('underscore@1.0.0', ['server', 'client']);
  api.use('accounts-base@2.2.1', ['client', 'server']);
  // Export Accounts (etc) to packages using this one.
  api.imply('accounts-base', ['client', 'server']);
  api.use('accounts-oauth@1.4.0', ['client', 'server']);

  api.use('communecter:oidcademe@1.1.0', ['client', 'server']);

  api.addFiles('oidcademe.js');
});
