var packageName = 'momentjs:moment';  // https://atmospherejs.com/momentjs/moment

Package.describe({
  name: packageName,
  summary: 'Moment.js (official): parse, validate, manipulate, and display dates - official Meteor packaging',
  version: '2.29.1',
  git: 'https://github.com/moment/moment.git'
});

Package.onUse(function (api) {
  api.versionsFrom(['METEOR@0.9.0', 'METEOR@1.0', 'METEOR@1.2']);
  api.use([
    'ecmascript',
  ]);

  api.mainModule('export_server.js', 'server');
  api.mainModule('export.js', 'client');
});
