
var FILES = {
  maptiler: {
    js: ['https://cdn.maptiler.com/maptiler-sdk-js/latest/maptiler-sdk.umd.min.js'],
    css: ['https://cdn.maptiler.com/maptiler-sdk-js/latest/maptiler-sdk.css'],
  },
};

var deps = new Deps.Dependency;
var loaded = false;

var onLoaded = function () {
  loaded = true;
  Maptiler.hook && Maptiler.hook.call && Maptiler.hook.call(this);
  deps.changed();
};

var onMapboxLoaded = function (plugins, cb) {
  var pluginCount = _.size(plugins);

  if (pluginCount === 0) {
    cb();
    return;
  }

  var loadCb = function () {
    pluginCount--;

    if (pluginCount === 0) {
      cb();
      return;
    }
  };

  _.each(plugins, function (plugin) {
    loadFiles(FILES[plugin], loadCb);
  });
};

var loadScript = function (src, cb) {
  var elem = document.createElement('script');
  elem.type = 'text/javascript';
  elem.src = src;
  elem.defer = true;

  elem.addEventListener('load', _.partial(cb, src), false);

  var head = document.getElementsByTagName('head')[0];
  head.appendChild(elem);
};

var loadCss = function (href) {
  var elem = document.createElement('link');
  elem.rel = 'stylesheet';
  elem.href = href;

  var head = document.getElementsByTagName('head')[0];
  head.appendChild(elem);
};

var loadFiles = function (files, cb) {
  var loadCount = _.size(files.js);

  var loadCb = function (url) {
    if (Maptiler.debug)
      console.log('Loaded:', url);

    loadCount--;

    if (loadCount === 0)
      cb();
  };

  _.each(files.css, loadCss);
  _.each(files.js, function (url) {
    loadScript(url, loadCb);
  });
};

Maptiler = {
  hook: null,
  debug: false,
  load: function (opts) {
    if (loaded)
      return;

    var opts = opts || {};
    var plugins = opts.plugins || [];
    var initialFiles = FILES.maptiler;

    loadFiles(initialFiles, _.partial(onMapboxLoaded, plugins, onLoaded));
  },
  loaded: function () {
    deps.depend();
    return loaded;
  },
  onLoaded: function (cb) {
    this.hook = cb;
  }
};
