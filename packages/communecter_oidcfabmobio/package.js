Package.describe({
  summary: 'OpenID Connect (OIDC) fabmobio',
  version: '1.1.0',
  name: 'communecter:oidcfabmobio',
});

Package.onUse(function (api) {
  api.use('oauth2@1.3.1', ['client', 'server']);
  api.use('oauth@2.1.0', ['client', 'server']);
  api.use('jkuester:http', 'server');
  api.use('underscore@1.0.0', ['server', 'client']);
  api.use('templating@1.1.0', 'client');
  api.use('random@1.0.0', 'client');
  api.use('service-configuration@1.0.0', ['client', 'server']);

  api.export('OidcFabmobio');

  api.addFiles('oidcfabmobio_server.js', 'server');
  api.addFiles('oidcfabmobio_client.js', 'client');
});
