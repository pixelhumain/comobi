/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/template-names */
/* global Router */
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import './qrcode.html';

Template._qrcode.helpers({
  shareUrl() {
    // return Meteor.absoluteUrl(window.location.pathname.replace(/\//, ''));
    const { scope } = Router.current().params;
    const qrLink = `https://www.communecter.org/#page.type.${scope}.id.${Router.current().params._id}`;

    return qrLink;
  },
  canShare() {
    return navigator.share;
  },
});