/* eslint-disable meteor/no-session */
/* global L IonLoading */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mapbox } from 'meteor/communecter:mapbox';
// import { Maptiler } from 'meteor/communecter:maptiler';
import { $ } from 'meteor/jquery';
import { Blaze } from 'meteor/blaze';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Poi } from '../../api/collection/poi.js';
import { Citoyens } from '../../api/collection/citoyens.js';

import { nameToCollection } from '../../api/helpers.js';
import { queryGeoFilter } from '../../api/helpers_client.js';

// submanager
import {
  listEventsSubs, listOrganizationsSubs, listProjectsSubs, listPoiSubs, listCitoyensSubs, scopeSubscribe,
} from '../../api/client/subsmanager.js';

import position from '../../api/client/position.js';
import { pageSession } from '../../api/client/reactive.js';

import './mapscope.html';

window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Poi = Poi;
window.Citoyens = Citoyens;

const subs = {};
subs.events = listEventsSubs;
subs.organizations = listOrganizationsSubs;
subs.projects = listProjectsSubs;
subs.poi = listPoiSubs;
subs.citoyens = listCitoyensSubs;

let clusters = [];
let map = [];
const markers = [];

const initialize = (element, zoom) => {
  const geo = position.getLatlng();
  const options = {
    maxZoom: 18,
    minZoom: 0,
  };
  if (geo && geo.latitude) {
    L.mapbox.accessToken = Meteor.settings.public.mapbox;
    const tiles = Meteor.settings.public.maptiler ? `https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=${Meteor.settings.public.maptiler}` : `https://api.mapbox.com/styles/v1/communecter/cj4ziz9st0re02qo4xtqu7puz/tiles/256/{z}/{x}/{y}?access_token=${Meteor.settings.public.mapbox}`;

    const tilejson = {
      tiles: [tiles],
      minzoom: 0,
      maxzoom: 18,
      attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
    };

    map = L.mapbox.map(element, tilejson)
      .setView(new L.LatLng(parseFloat(geo.latitude), parseFloat(geo.longitude)), zoom);

    if (Meteor.settings.public.maptiler) {
      const mapboxTiles = L.tileLayer(tiles, {
        attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
      });
      map.addLayer(mapboxTiles, options);
    } else {
      map.addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11', options));
    }

    const marker = L.marker(new L.LatLng(parseFloat(geo.latitude), parseFloat(geo.longitude)), {
      icon: L.mapbox.marker.icon({
        'marker-size': 'small',
        'marker-color': '#fa0',
      }),
    }).bindPopup('Vous êtes ici :)');
    map.addLayer(marker);

    const radius = position.getRadius();
    const latlngObj = position.getLatlngObject();
    if (radius && latlngObj) {
      // circle radius
      // const filterCircle = L.circle([parseFloat(geo.latitude), parseFloat(geo.longitude)], radius, {
      //   opacity: 0.1,
      //   weight: 1,
      //   fillOpacity: 0.1,
      // }).addTo(map);
      // if (!Session.get('currentScopeId')) {
      //   map.fitBounds(filterCircle.getBounds());
      // }

      // polygon radius
      const nearObj = position.getBoundingBox();
      const latlngs = [[nearObj.yMin, nearObj.xMin], [nearObj.yMax, nearObj.xMin], [nearObj.yMax, nearObj.xMax], [nearObj.yMin, nearObj.xMax]];
      const polygon = L.polygon(latlngs, {
        opacity: 0.2,
        weight: 1,
        fillOpacity: 0.2,
      });//.addTo(map);

      if (!Session.get('currentScopeId')) {
        // zoom the map to the polygon
        map.fitBounds(polygon.getBounds());
      }

    } else {
      const city = position.getCity();
      if (city && city.geoShape && city.geoShape.coordinates) {
        const geojson = [
          {
            type: 'Feature',
            geometry: city.geoShape,
            properties: {
              opacity: 0.1,
              weight: 1,
              fillOpacity: 0.1,
            },
          },
        ];
        const polygon = L.geoJson(geojson, { style: L.mapbox.simplestyle.style }).addTo(map);
        if (!Session.get('currentScopeId')) {
          map.fitBounds(polygon.getBounds());
        }
      }
    }

    clusters = new L.MarkerClusterGroup();

    map.addLayer(clusters);
  }
};

const addMarker = (marker) => {
  // map.addLayer(marker);
  clusters.addLayer(marker);
  markers[marker.options._id] = marker;
  if (pageSession.get('currentScopeId') === marker.options._id) {
    // console.log('marker open');
    map.panTo([marker.options.latitude, marker.options.longitude]);
    marker.addTo(map).openPopup();
    map.on('popupclose', function () {
      // console.log('popupclose');
      // map.removeLayer(marker);
    });
  }
};

const removeMarker = (_id) => {
  const marker = markers[_id];
  if (clusters.hasLayer(marker)) clusters.removeLayer(marker);
};

const clearLayers = () => {
  clusters.clearLayers();
};

const selectIcon = (event) => {
  if (event && event.profilMarkerImageUrl) {
    return L.icon({
      iconUrl: `${Meteor.settings.public.urlimage}${event.profilMarkerImageUrl}`,
      iconSize: [53, 60], // 38, 95],
      iconAnchor: [27, 57], // 22, 94],
      popupAnchor: [0, -55], // -3, -76]
    });
  }

  const icoMarkersMap = {
    default: '',

    city: 'city-marker-default',

    news: 'NEWS_A',
    idea: 'NEWS_A',
    question: 'NEWS_A',
    announce: 'NEWS_A',
    information: 'NEWS_A',

    citoyen: 'citizen-marker-default',
    citoyens: 'citizen-marker-default',
    people: 'citizen-marker-default',

    NGO: 'ngo-marker-default',
    organizations: 'ngo-marker-default',
    organization: 'ngo-marker-default',

    event: 'event-marker-default',
    events: 'event-marker-default',
    meeting: 'event-marker-default',

    project: 'project-marker-default',
    projects: 'project-marker-default',

    markerPlace: 'map-marker',

    poi: 'poi-marker-default',
    'poi.video': 'poi-video-marker-default',
    'poi.link': 'poi-marker-default',
    'poi.geoJson': 'poi-marker-default',
    'poi.compostPickup': 'poi-marker-default',
    'poi.sharedLibrary': 'poi-marker-default',
    'poi.artPiece': 'poi-marker-default',
    'poi.recoveryCenter': 'poi-marker-default',
    'poi.trash': 'poi-marker-default',
    'poi.history': 'poi-marker-default',
    'poi.something2See': 'poi-marker-default',
    'poi.funPlace': 'poi-marker-default',
    'poi.place': 'poi-marker-default',
    'poi.streetArts': 'poi-marker-default',
    'poi.openScene': 'poi-marker-default',
    'poi.stand': 'poi-marker-default',
    'poi.parking': 'poi-marker-default',

    entry: 'entry-marker-default',
    action: 'action-marker-default',

    url: 'url-marker-default',

    address: 'MARKER',

    classified: 'classified-marker-default',

  };
  // const assetPath = '/assets/feadb1ba';
  // const iconUrl = `${Meteor.settings.public.assetPath}/images/sig/markers/icons_carto/${icoMarkersMap[Router.current().params.scope]}.png`;
  // return L.icon({
  //   iconUrl: `${Meteor.settings.public.urlimage}${iconUrl}`,
  //   iconSize: [53, 60], // 38, 95],
  //   iconAnchor: [27, 57], // 22, 94],
  //   popupAnchor: [0, -55], // -3, -76]
  // });

  const iconUrl = `/sig/markers/${icoMarkersMap[Router.current().params.scope]}.png`;
  return L.icon({
    iconUrl: `${iconUrl}`,
    iconSize: [53, 60], // 38, 95],
    iconAnchor: [27, 57], // 22, 94],
    popupAnchor: [0, -55], // -3, -76]
  });
};

/* const selectColor = (event) => {
  const inputDate = new Date();
  if (event.startDate < inputDate && event.endDate < inputDate) {
    return '#cccccc';
  } else if (event.startDate <= inputDate && event.endDate > inputDate) {
    return '#33cd5f';
  }
  return '#324553';
}; */

Template.mapCanvas.onCreated(function () {
  const self = this;
  scopeSubscribe(this, subs[Router.current().params.scope], 'geo.scope', Router.current().params.scope);

  self.readyDetail = new ReactiveVar();
  self.autorun(function () {
    if (Router.current().params._id) {
      pageSession.set('currentScopeId', Router.current().params._id);
      const handleDetail = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
      if (handleDetail.ready()) {
        self.readyDetail.set(handleDetail.ready());
      }
    }
  });
});

Template.mapCanvas.onRendered(function () {
  const self = this;
  $(window).resize(function () {
    const h = $(window).height();
    const offsetTop = 40;
    $('#map_canvas').css('height', (h - offsetTop));
  }).resize();

  self.autorun(function (c) {
    IonLoading.show();
    if (self.ready.get()) {
      if (Mapbox.loaded()) {
        IonLoading.hide();
        c.stop();
      }
    }
  });

  self.autorun(function (c) {
    // if (self.ready.get()) {
    if (Mapbox.loaded()) {
      initialize($('#map_canvas')[0], 13);
      c.stop();
    }
    // }
  });

  if (self.liveQuery) {
    self.liveQuery.stop();
  }
  if (self.liveQueryCurrent) {
    self.liveQueryCurrent.stop();
  }
  self.autorun(function () {
    if (self.ready.get()) {
      if (Mapbox.loaded()) {
        clearLayers();

        const collection = nameToCollection(Router.current().params.scope);

        if (pageSession.get('currentScopeId')) {
          self.liveQueryCurrent = collection.find({ _id: new Mongo.ObjectID(pageSession.get('currentScopeId')), geo: { $exists: 1 } }).observe({
            added(event) {
              if (event && event.geo && event.geo.latitude) {
                const containerNode = document.createElement('div');

                Blaze.renderWithData(Template.mapscopepopup, event, containerNode);
                const marker = new L.Marker([event.geo.latitude, event.geo.longitude], {
                  _id: event._id._str,
                  title: event.name,
                  latitude: event.geo.latitude,
                  longitude: event.geo.longitude,
                  icon: selectIcon(event),
                }).bindPopup(containerNode).on('click', function (e) {
                  // console.log(e.target.options._id);
                  map.panTo([e.target.options.latitude, e.target.options.longitude]);
                  // pageSession.set('currentScopeId', e.target.options._id);
                });
                addMarker(marker);
              }
            },
            changed(event) {
              // console.log(event._id._str);
              if (event && event.geo && event.geo.latitude) {
                const marker = markers[event._id._str];
                if (marker) {
                  if (map.hasLayer(marker)) map.removeLayer(marker);
                  const containerNode = document.createElement('div');
                  Blaze.renderWithData(Template.mapscopepopup, event, containerNode);
                  const markerAdd = new L.Marker([event.geo.latitude, event.geo.longitude], {
                    _id: event._id._str,
                    title: event.name,
                    latitude: event.geo.latitude,
                    longitude: event.geo.longitude,
                    icon: selectIcon(event),
                  }).bindPopup(containerNode).on('click', function (e) {
                    // console.log(e.target.options._id);
                    map.panTo([e.target.options.latitude, e.target.options.longitude]);
                    // pageSession.set('currentScopeId', e.target.options._id);
                  });
                  addMarker(markerAdd);
                }
              }
            },
            removed(event) {
              // console.log(event._id._str);
              removeMarker(event._id._str);
            },
          });
        }

        let query = {};
        query = queryGeoFilter(query);
        // query['created'] = {$gte : inputDate};

        /* if (Router.current().params.scope === 'events') {

        } */
        query.geo = { $exists: 1 };

        self.liveQuery = collection.find(query).observe({
          added(event) {
            if (event && event.geo && event.geo.latitude) {
              const containerNode = document.createElement('div');

              Blaze.renderWithData(Template.mapscopepopup, event, containerNode);
              const marker = new L.Marker([event.geo.latitude, event.geo.longitude], {
                _id: event._id._str,
                title: event.name,
                latitude: event.geo.latitude,
                longitude: event.geo.longitude,
                icon: selectIcon(event),
              }).bindPopup(containerNode).on('click', function (e) {
                // console.log(e.target.options._id);
                map.panTo([e.target.options.latitude, e.target.options.longitude]);
                // pageSession.set('currentScopeId', e.target.options._id);
              });
              addMarker(marker);
            }
          },
          changed(event) {
            // console.log(event._id._str);
            if (event && event.geo && event.geo.latitude) {
              const marker = markers[event._id._str];
              if (marker) {
                if (map.hasLayer(marker)) map.removeLayer(marker);
                const containerNode = document.createElement('div');
                Blaze.renderWithData(Template.mapscopepopup, event, containerNode);
                const markerAdd = new L.Marker([event.geo.latitude, event.geo.longitude], {
                  _id: event._id._str,
                  title: event.name,
                  latitude: event.geo.latitude,
                  longitude: event.geo.longitude,
                  icon: selectIcon(event),
                }).bindPopup(containerNode).on('click', function (e) {
                  // console.log(e.target.options._id);
                  map.panTo([e.target.options.latitude, e.target.options.longitude]);
                  // pageSession.set('currentScopeId', e.target.options._id);
                });
                addMarker(markerAdd);
              }
            }
          },
          removed(event) {
            // console.log(event._id._str);
            removeMarker(event._id._str);
          },
        });
      }
    }
  });
});

Template.mapCanvas.onDestroyed(function () {
  const self = this;
  // console.log('destroyed');
  pageSession.set('currentScopeId', false);
  // map.remove();
  if (self.liveQuery) {
    self.liveQuery.stop();
  }
  if (self.liveQueryCurrent) {
    self.liveQueryCurrent.stop();
  }
});
