import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Mongo } from 'meteor/mongo';

// submanager
import { collectionsListSubs } from '../../api/client/subsmanager.js';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Poi } from '../../api/collection/poi.js';

import { nameToCollection } from '../../api/helpers.js';

import { pageCollections } from '../../api/client/reactive.js';

import '../components/directory/list.js';

import './collections.html';

window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Citoyens = Citoyens;
window.Poi = Poi;

// suivant le scope

Template.collections.onCreated(function () {
  this.ready = new ReactiveVar();
  pageCollections.set('search', null);
  this.autorun(function () {
    pageCollections.set('scopeId', Router.current().params._id);
    pageCollections.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = collectionsListSubs.subscribe('collectionsList', Router.current().params.scope, Router.current().params._id, 'favorites');
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.collections.onRendered(function () {
});

Template.collections.helpers({
  scope() {
    if (Router.current().params.scope) {
      const collection = nameToCollection(Router.current().params.scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    }
    return undefined;
  },
  scopeCollectionsTemplate() {
    return `listCollections${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.collectionsView.onCreated(function () {
  pageCollections.set('search', null);
  pageCollections.set('view', 'all');
});

Template.collectionsView.helpers({
  search() {
    return pageCollections.get('search');
  },
  viewArray() {
    return ['citoyens', 'organizations', 'projects', 'events', 'poi'];
  },
  view() {
    return pageCollections.get('view');
  },
});

Template.collectionsSearch.events({
  'keyup #search, change #search': _.throttle((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageCollections.set('search', event.currentTarget.value);
    } else {
      pageCollections.set('search', null);
    }
  }, 500),
});

Template.collectionsButtonBar.events({
  'click .all'(event) {
    event.preventDefault();
    pageCollections.set('view', 'all');
  },
  'click .citoyens'(event) {
    event.preventDefault();
    pageCollections.set('view', 'citoyens');
  },
  'click .organizations'(event) {
    event.preventDefault();
    pageCollections.set('view', 'organizations');
  },
  'click .poi'(event) {
    event.preventDefault();
    pageCollections.set('view', 'poi');
  },
  'click .events'(event) {
    event.preventDefault();
    pageCollections.set('view', 'events');
  },
  'click .projects'(event) {
    event.preventDefault();
    pageCollections.set('view', 'projects');
  },
});
