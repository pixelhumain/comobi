/* eslint-disable no-underscore-dangle */
/* global L maptilersdk */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Mapbox } from 'meteor/communecter:mapbox';
// import { Maptiler } from 'meteor/communecter:maptiler';
import { AutoForm } from 'meteor/aldeed:autoform';

import { pageSession } from '../../api/client/reactive.js';

import './map.html';

Template.map.onCreated(function () {

});

Template.map.onRendered(function () {
  if (Mapbox.loaded()) {
    const self = this;
    L.mapbox.accessToken = Meteor.settings.public.mapbox;
    const tiles = Meteor.settings.public.maptiler ? `https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=${Meteor.settings.public.maptiler}` : `https://api.mapbox.com/styles/v1/communecter/cj4ziz9st0re02qo4xtqu7puz/tiles/256/{z}/{x}/{y}?access_token=${Meteor.settings.public.mapbox}`;

    const tilejson = {
      tiles: [tiles],
      minzoom: 0,
      maxzoom: 18,
      attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
    };

    // const map = L.mapbox.map('map', 'mapbox.streets');
    const map = L.mapbox.map('map', tilejson);
    let marker;
    self.autorun(function () {
      const latitude = pageSession.get('geoPosLatitude') || AutoForm.getFieldValue('geoPosLatitude');
      const longitude = pageSession.get('geoPosLongitude') || AutoForm.getFieldValue('geoPosLongitude');
      // console.log(`${city} ${latitude} ${longitude}`);
      if (latitude && longitude) {
        // console.log('recompute');
        map.setView(new L.LatLng(parseFloat(latitude), parseFloat(longitude)), 13);
        if (Meteor.settings.public.maptiler) {
          const mapboxTiles = L.tileLayer(tiles, {
            attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
          });
          map.addLayer(mapboxTiles);
        } else {
          map.addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
        }
        if (marker) {
          map.removeLayer(marker);
        }
        marker = L.marker(new L.LatLng(parseFloat(latitude), parseFloat(longitude)), {
          id: 'point',
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          draggable: true,
          icon: L.mapbox.marker.icon({
            'marker-size': 'large',
            'marker-color': '#fa0',
          }),
        }).addTo(map);

        marker.on('dragend', function (e) {
          // console.log('dragend');
          pageSession.set('geoPosLatitude', e.target._latlng.lat);
          pageSession.set('geoPosLongitude', e.target._latlng.lng);
        });
      }
    });
  }
});
