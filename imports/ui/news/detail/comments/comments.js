/* global IonToast IonActionSheet */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { AutoForm } from 'meteor/aldeed:autoform';

// collection
import { Events } from '../../../../api/collection/events.js';
import { Organizations } from '../../../../api/collection/organizations.js';
import { Projects } from '../../../../api/collection/projects.js';
import { Citoyens } from '../../../../api/collection/citoyens.js';
import { Comments } from '../../../../api/collection/comments.js';

// submanager
// import { singleSubs } from '../../../../api/client/subsmanager.js';

import { nameToCollection } from '../../../../api/helpers.js';

import { pageSession } from '../../../../api/client/reactive.js';

import './comments.html';

window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Citoyens = Citoyens;

Template.newsDetailComments.onCreated(function () {
  const template = Template.instance();
  template.ready = new ReactiveVar();
  template.scope = Router.current().params.scope;
  template._id = Router.current().params._id;
  template.newsId = Router.current().params.newsId;
  this.autorun(function () {
    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  this.autorun(function () {
    if (template.scope && template._id && template.newsId) {
      const handleMe = Meteor.subscribe('citoyen');
      const handle = Meteor.subscribe('scopeDetail', template.scope, template._id);
      const handleScopeDetail = Meteor.subscribe('newsDetailComments', template.scope, template._id, template.newsId);
      if (handle.ready() && handleScopeDetail.ready() && handleMe.ready()) {
        template.ready.set(handle.ready());
      }
    }
  });
});

Template.newsDetailComments.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id && Template.instance().newsId && Template.instance().ready.get()) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    return undefined;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.newsDetailComments.events({
  'click .action-comment'(event) {
    const self = this;
    event.preventDefault();
    IonActionSheet.show({
      titleText: i18n.__('Actions Comment'),
      buttons: [
        { text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      ],
      destructiveText: i18n.__('delete'),
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('commentsEdit', {
            _id: Router.current().params._id, newsId: Router.current().params.newsId, scope: Router.current().params.scope, commentId: self._id._str,
          });
        }
        return true;
      },
      destructiveButtonClicked() {
        // console.log('Destructive Action!');
        Meteor.call('deleteComment', self._id._str, function () {
          Router.go('newsDetail', { _id: Router.current().params._id, newsId: Router.current().params.newsId, scope: Router.current().params.scope }, { replaceState: true });
        });
        return true;
      },
    });
  },
  'click .like-comment'(event) {
    Meteor.call('likeScope', this._id._str, 'comments');
    event.preventDefault();
  },
  'click .dislike-comment'(event) {
    Meteor.call('dislikeScope', this._id._str, 'comments');
    event.preventDefault();
  },
});

Template.newsDetailCommentsData.events({
  'click .action-news-flags'(event) {
    const self = this;
    event.preventDefault();
    if (self.isAuthor()) {
      IonPopup.alert({
        title: i18n.__('Reporting not possible'),
        template: i18n.__('This news was written by you, it is not possible to use the report on your own content'),
      });
      return;
    }
    // _id=target.id newsId=_id._str scope=target.type
    IonActionSheet.show({
      titleText: i18n.__('Actions report / flag'),
      buttons: [
        { text: `${i18n.__('report users')} <i class="icon ion-flag"></i>` },
        { text: `${i18n.__('report violating content')} <i class="icon ion-flag"></i>` },
        { text: `${i18n.__('block user')} <i class="icon ion-flag"></i>` },
      ],
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // report user
          const authorComments = self.authorComments() ?? null;
          const cibleId = authorComments._id._str;
          const cibleScope = 'citoyens';
          IonModal.open('_reportflag', { cibleId, cibleScope });
        }
        if (index === 1) {
          // report violating content
          IonModal.open('_reportflag', { cibleScope: 'comments', cibleId: self._id._str });
        }
        if (index === 2) {
          // block user
          const authorComments = self.authorComments() ?? null;
          const cibleId = authorComments._id._str;
          const cibleScope = 'citoyens';

          IonPopup.confirm({
            title: i18n.__('block user/element'),
            template: i18n.__('Are you sure you want to block this user/element ?'),
            onOk() {
              Meteor.call('blockUser', cibleId, cibleScope, (error) => {
                if (error) {
                  IonPopup.alert({ template: i18n.__(error.reason) });
                } else {
                  // window.history.back();
                  // Router.go('newsDetail', { _id: Router.current().params._id, newsId: Router.current().params.newsId, scope: Router.current().params.scope }, { replaceState: true });
                }
              });
            },
            onCancel() {

            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
        return true;
      },
    });
  },
});

Template.commentsAdd.onCreated(function () {
  this.autorun(function () {
    pageSession.set('newsId', Router.current().params.newsId);
  });

  pageSession.set('error', false);
});

Template.commentsAdd.onRendered(function () {
  const self = this;
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);
});

Template.commentsAdd.helpers({
  error() {
    return pageSession.get('error');
  },
});

Template.commentsAdd.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

Template.commentsEdit.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);

  self.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('newsId', Router.current().params.newsId);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
    const handleScopeDetail = Meteor.subscribe('newsDetailComments', Router.current().params.scope, Router.current().params._id, Router.current().params.newsId);
    if (handle.ready() && handleScopeDetail.ready()) {
      self.ready.set(handle.ready());
    }
  });
});

Template.commentsEdit.onRendered(function () {
  const self = this;
});

Template.commentsEdit.helpers({
  comment() {
    const comment = Comments.findOne({ _id: new Mongo.ObjectID(Router.current().params.commentId) });
    if (comment && comment.mentions) {
      pageSession.set('mentions', comment.mentions);
    }
    comment._id = comment._id._str;
    return comment;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.commentsEdit.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

AutoForm.addHooks(['addComment', 'editComment'], {
  before: {
    method(doc) {
      const newsId = pageSession.get('newsId');
      doc.contextType = 'news';
      doc.contextId = newsId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => doc.text.match(`@${array.value}`) !== null);
        doc.mentions = arrayMentions;
      }
      return doc;
    },
    'method-update'(modifier) {
      const newsId = pageSession.get('newsId');
      modifier.$set.contextType = 'news';
      modifier.$set.contextId = newsId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => modifier.$set.text.match(`@${array.value}`) !== null);
        modifier.$set.mentions = arrayMentions;
      }
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          template: i18n.__('error'), position: 'top', type: 'error', showClose: true, title: `<i class="icon ion-alert"></i> ${error && error.reason ? i18n.__(error.reason.replace(': ', '')) : i18n.__(error.error)}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('error'), position: 'top', type: 'error', showClose: true, title: `<i class="icon ion-alert"></i> ${error && error.reason ? i18n.__(error.reason.replace(': ', '')) : i18n.__(error.error)}`,
        });
      }
    }
  },
});

AutoForm.addHooks(['editComment', 'addComment'], {
  after: {
    method(error) {
      if (!error) {
        // translate todo
        IonToast.show({
          template: 'Commentaire ajouté', position: 'bottom', type: 'success', showClose: false, title: '<i class="icon ion-checkmark"></i> Commentaire',
        });
      }
    },
    'method-update'(error) {
      if (!error) {
        // translate todo
        IonToast.show({
          template: 'Commentaire modifié', position: 'bottom', type: 'success', showClose: false, title: '<i class="icon ion-checkmark"></i> commentaire',
        });
        Router.go('newsDetailComments', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), newsId: pageSession.get('newsId') }, { replaceState: true });
      }
    },
  },
});
