import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import i18n from 'meteor/universe:i18n';
import { Router } from 'meteor/iron:router';
import { IonPopup } from 'meteor/meteoric:ionic';

import { AutoForm } from 'meteor/aldeed:autoform';
import Tribute from 'tributejs';
import { pageSession } from '../../api/client/reactive.js';

import position from '../../api/client/position.js';

// collections
import { Citoyens } from '../../api/collection/citoyens.js';

import './global.html';

Template.testgeo.onRendered(function () {
  const testgeo = () => {
    const geolocate = position.getGeolocate();
    if (!position.getGPSstart() && geolocate && !position.getReactivePosition()) {
      IonPopup.confirm({
        title: i18n.__('Location'),
        template: i18n.__('Use the location of your profile'),
        onOk() {
          if (Citoyens.findOne() && Citoyens.findOne().geo && Citoyens.findOne().geo.latitude) {
            position.setMockLocation(Citoyens.findOne().geo);
            position.setGeolocate(false);
          }
        },
        onCancel() {
          Router.go('changePosition');
        },
        cancelText: i18n.__('no'),
        okText: i18n.__('yes'),
      });
    }
  };

  Meteor.setTimeout(testgeo, '3000');
});

Template.cityTitle.onCreated(function () {
  const self = this;
  self.autorun(function () {
    const latlngObj = position.getLatlngObject();
    if (latlngObj) {
      Meteor.call('getcitiesbylatlng', latlngObj, function (error, result) {
        if (result) {
          // console.log('call city');
          position.setCity(result);
        }
      });
    }
  });
});

Template.cityTitle.helpers({
  city() {
    return position.getCity();
  },
});

AutoForm.addInputType('textareaTribute', {
  template: 'afTextareaTribute',
  valueIn(val, atts) {
    return val;
  },
  valueOut() {
    return this.val();
  },
});

// textarea tribute fields mentions
Template.afTextareaTribute.onRendered(function () {
  const self = this;
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);
  self.tributeMultipleTriggers = new Tribute({
    collection: [
      {
        trigger: '@',
        selectTemplate(item) {
          const mentions = {};
          mentions.name = item.original.name;
          mentions.id = item.original.id;
          mentions.type = item.original.type;
          mentions.avatar = item.original.avatar;
          mentions.value = (item.original.slug ? item.original.slug : item.original.name);
          mentions.slug = (item.original.slug ? item.original.slug : null);
          if (pageSession.get('mentions')) {
            const arrayMentions = pageSession.get('mentions');
            arrayMentions.push(mentions);
            pageSession.set('mentions', arrayMentions);
          } else {
            pageSession.set('mentions', [mentions]);
          }
          return `@${item.original.slug}`;
        },
        menuItemTemplate(item) {
          return item.original.avatar ? `<img src='${item.original.avatar}' height='20' width='20'/> ${item.string}` : `${item.string}`;
        },
        autocompleteMode: true,
        menuItemLimit: 20,
        allowSpaces: true,
        requireLeadingSpace: true,
        menuShowMinLength: 2,
        values(text, cb) {
          if (pageSession.get('queryMention') !== text) {
            pageSession.set('queryMention', text);
            const querySearch = {};
            querySearch.search = text;
            Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
              if (!error) {
                const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? { id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}` } : { id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens' }));
                const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? { id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}` } : { id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations' }));
                const arrayUnions = _.union(citoyensArray, organizationsArray);
                cb(arrayUnions);
              }
            });
          }
        },
        lookup: 'name',
        fillAttr: 'name',
      },
    ],
  });
  self.tributeMultipleTriggers.attach(self.findAll('.atwho-inputor'));
});

Template.afTextareaTribute.helpers({
  atts: function addFormControlAtts() {
    const atts = _.clone(this.atts);
    return atts;
  },
});

Template.afTextareaTribute.onDestroyed(function () {
  const self = this;
  self.tributeMultipleTriggers.detach(self.findAll('.atwho-inputor'));
});
