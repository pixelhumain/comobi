import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Citoyens = new Mongo.Collection('citoyens', { idGeneration: 'MONGO' });
