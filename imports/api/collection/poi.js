import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Poi = new Mongo.Collection('poi', { idGeneration: 'MONGO' });
