import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const News = new Mongo.Collection('news', { idGeneration: 'MONGO' });
