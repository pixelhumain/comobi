import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Gamesmobile = new Mongo.Collection('gamesmobile', { idGeneration: 'MONGO' });

export const Playersmobile = new Mongo.Collection('playersmobile', { idGeneration: 'MONGO' });

export const Questsmobile = new Mongo.Collection('questsmobile', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  // Index
  Playersmobile.rawCollection().createIndex(
    { idUser: 1, idGame: 1 },
    { name: 'usergame', background: true, unique: true },
    (e) => {
      if (e) {
        // console.log(e);
      }
    },
  );
  Questsmobile.rawCollection().createIndex(
    { idGame: 1 },
    { name: 'questgame', background: true },
    (e) => {
      if (e) {
        // console.log(e);
      }
    },
  );
}
