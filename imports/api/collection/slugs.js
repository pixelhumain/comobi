import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Slugs = new Mongo.Collection('slugs', { idGeneration: 'MONGO' });
