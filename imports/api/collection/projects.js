import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Projects = new Mongo.Collection('projects', { idGeneration: 'MONGO' });
