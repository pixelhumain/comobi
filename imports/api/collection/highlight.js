import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Highlight = new Mongo.Collection('highlight', { idGeneration: 'MONGO' });
