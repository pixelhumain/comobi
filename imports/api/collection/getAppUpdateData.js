import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const AppUpdatesCollection = new Mongo.Collection('comobiapppdates', { idGeneration: 'MONGO' });
