import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Cities = new Mongo.Collection('cities', { idGeneration: 'MONGO' });
