import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

// collection
import { Comments } from '../collection/comments.js';
import { Citoyens } from '../collection/citoyens.js';

Comments.helpers({
  authorComments() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.author) });
  },
  likesCount() {
    if (this.voteUp && this.voteUpCount) {
      return this.voteUpCount;
    }
    return 0;
  },
  dislikesCount() {
    if (this.voteDown && this.voteDownCount) {
      return this.voteDownCount;
    }
    return 0;
  },
  isAuthor() {
    return this.author === Meteor.userId();
  },
  isReportAbuse() {
    return !!(this.reportAbuse && this.reportAbuse[Meteor.userId()]);
  },
});

if (Meteor.isClient) {
  Comments.helpers({
    dateComments() {
      return moment.unix(this.created).format('YYYY-MM-DD HH:mm');
    },
    textMentions() {
      if (this.text) {
        let text = this.text;
        if (this.mentions) {
          Object.values(this.mentions).forEach((array) => {
            // text = text.replace(new RegExp(`@${array.value}`, 'g'), `<a href="${Router.path('detailList', {scope:array.type,_id:array.id})}" class="positive">@${array.value}</a>`);
            if (array.slug) {
              text = text.replace(new RegExp(`@?${array.slug}`, 'g'), `<a href="${Router.path('detailList', { scope: array.type, _id: array.id })}" class="positive">@${array.slug}</a>`);
            } else {
              text = text.replace(new RegExp(`@?${array.value}`, 'g'), `<a href="${Router.path('detailList', { scope: array.type, _id: array.id })}" class="positive">@${array.value}</a>`);
            }
          });
        }
        return text;
      }
      return undefined;
    },
    classArgval() {
      if (this.argval === 'up') {
        return 'item-balanced';
      } else if (this.argval === 'white') {
        return 'item-stable';
      } else if (this.argval === 'down') {
        return 'item-assertive';
      }
      return null;
    },
  });
}
