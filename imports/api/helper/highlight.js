import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';

// schemas
import { Highlight } from '../collection/highlight.js';
import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { Events } from '../collection/events.js';
import { Gamesmobile } from '../collection/gamemobile.js';
import { nameToCollection } from '../helpers.js';

if (Meteor.isClient) {
  window.Organizations = Organizations;
  window.Projects = Projects;
  window.Events = Events;
  window.Gamesmobile = Gamesmobile;
}

Highlight.helpers({
  parentHighlight() {
    if (this.parentId && this.parentType && _.contains(['projects', 'organizations', 'events', 'gamesmobile'], this.parentType)) {
      // console.log(this.parentType);
      const collectionType = nameToCollection(this.parentType);
      return collectionType.findOne({
        _id: new Mongo.ObjectID(this.parentId),
      }, {
        fields: {
          link: 0,
        },
      });
    }
    return undefined;
  },
});
