import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

// collection
import { Poi } from '../collection/poi.js';
import { Citoyens } from '../collection/citoyens.js';
import { Organizations } from '../collection/organizations.js';
import { Documents } from '../collection/documents.js';
import { Events } from '../collection/events.js';
import { Projects } from '../collection/projects.js';
import { arrayOrganizerParent, isAdminArray, nameToCollection } from '../helpers.js';

if (Meteor.isClient) {
  window.Organizations = Organizations;
  window.Citoyens = Citoyens;
  window.Projects = Projects;
  window.Events = Events;
}

Poi.helpers({
  isVisibleFields(field) {
    /* if(this.isMe()){
        return true;
      }else{
        if(this.isPublicFields(field)){
          return true;
        }else{
          if(this.isFollowersMe() && this.isPrivateFields(field)){
            return true;
          }else{
            return false;
          }
        }
      } */
    return true;
  },
  documents() {
    return Documents.find({
      id: this._id._str,
      contentKey: 'profil',
    }, { sort: { created: -1 }, limit: 1 });
  },
  organizerPoi() {
    if (this.parent) {
      const childrenParent = arrayOrganizerParent(this.parent, ['events', 'projects', 'organizations', 'citoyens']);
      if (childrenParent) {
        return childrenParent;
      }
    }
    return undefined;
  },
  creatorProfile() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.creator) });
  },
  isCreator() {
    return this.creator === Meteor.userId();
  },
  isFavorites(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) }).isFavorites('poi', this._id._str);
  },
  isAdmin(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();

    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) });
    const organizerPoi = this.organizerPoi();

    if (bothUserId && this.parent) {
      if (this.parent[bothUserId] && this.parent[bothUserId].type === 'citoyens') {
        return true;
      }
      // console.log(this.organizerPoi());
      return isAdminArray(organizerPoi, citoyen);
    }
    return undefined;
  },
  scopeVar() {
    return 'poi';
  },
  scopeEdit() {
    return 'poiEdit';
  },
  listScope() {
    return 'listPoi';
  },
});
