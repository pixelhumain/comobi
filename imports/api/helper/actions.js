import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { _ } from 'meteor/underscore';

import { Actions } from '../collection/actions.js';
import { Citoyens } from '../collection/citoyens.js';
import { Comments } from '../collection/comments.js';
import { queryLink, queryLinkToBeValidated, queryOptions } from '../helpers.js';

if (Meteor.isClient) {
  import { Chronos } from '../client/chronos.js';

  Actions.helpers({
    isEndDate() {
      const end = moment(this.endDate).toDate();
      return Chronos.moment(end).isBefore(); // True
    },
    isNotEndDate() {
      const end = moment(this.endDate).toDate();
      return Chronos.moment().isBefore(end); // True
    },
  });
} else {
  Actions.helpers({
    isEndDate() {
      const end = moment(this.endDate).toDate();
      return moment(end).isBefore(); // True
    },
    isNotEndDate() {
      const end = moment(this.endDate).toDate();
      return moment().isBefore(end); // True
    },
  });
}

Actions.helpers({
  isVisibleFields(field) {
    return true;
  },
  isPublicFields(field) {
    return this.preferences && this.preferences.publicFields && _.contains(this.preferences.publicFields, field);
  },
  isPrivateFields(field) {
    return this.preferences && this.preferences.privateFields && _.contains(this.preferences.privateFields, field);
  },
  scopeVar() {
    return 'actions';
  },
  scopeEdit() {
    return 'actionsEdit';
  },
  listScope() {
    return 'listActions';
  },
  creatorProfile() {
    return Citoyens.findOne({
      _id: new Mongo.ObjectID(this.creator),
    }, {
      fields: {
        name: 1,
        profilThumbImageUrl: 1,
      },
    });
  },
  isCreator() {
    return this.creator === Meteor.userId();
  },
  listMembersToBeValidated() {
    if (this.links && this.links.contributors) {
      const query = queryLinkToBeValidated(this.links.contributors);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  isContributors(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return !!((this.links && this.links.contributors && this.links.contributors[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId)));
  },
  listContributors(search) {
    if (this.links && this.links.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countContributors(search) {
    // return this.links && this.links.contributors && _.size(this.links.contributors);
    return this.listContributors(search) && this.listContributors(search).count();
  },
  isToBeValidated(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return !((this.links && this.links.contributors && this.links.contributors[bothUserId] && this.links.contributors[bothUserId].toBeValidated));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links && this.links[scope] && this.links[scope][scopeId] && this.links[scope][scopeId].isInviting));
  },
  momentStartDate() {
    return moment(this.startDate).toDate();
  },
  momentEndDate() {
    return moment(this.endDate).toDate();
  },
  formatStartDate() {
    return moment(this.startDate).format('DD/MM/YYYY HH:mm');
  },
  formatEndDate() {
    return moment(this.endDate).format('DD/MM/YYYY HH:mm');
  },
  listComments() {
    // console.log('listComments');
    return Comments.find({
      contextId: this._id._str,
    }, { sort: { created: -1 } });
  },
  commentsCount() {
    if (this.commentCount) {
      return this.commentCount;
    }
    return 0;
  },
});
