import { Meteor } from 'meteor/meteor';
import position from './client/position.js';

export const queryGeoFilter = (query) => {
    const radius = position.getRadius();
    const latlngObj = position.getLatlngObject();
    if (radius && latlngObj) {
        const nearObj = position.getNear();
        query = { ...query, ...nearObj };
    } else {
        const city = position.getCity();
        if (city && city.geoShape && city.geoShape.coordinates) {
            query['address.codeInsee'] = city.insee;
        }
    }
    return query;
};

export const userLanguage = () => {
    // If the user is logged in, retrieve their saved language
    if (Meteor.user()) {
        return Meteor.user().profile.language;
    }
    return undefined;
};

export const languageBrowser = () => {
    const localeFromBrowser = window.navigator.userLanguage || window.navigator.language;
    let locale = 'en';

    if (localeFromBrowser.match(/en/)) locale = 'en';
    if (localeFromBrowser.match(/fr/)) locale = 'fr';

    return locale;
};
