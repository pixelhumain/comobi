import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { _ } from 'meteor/underscore';
import { HTTP } from 'meteor/jkuester:http';
import { Random } from 'meteor/random';
import { Mongo } from 'meteor/mongo';

// collection
import { ActivityStream, ActivityStreamReference } from '../collection/activitystream.js';
import { Citoyens } from '../collection/citoyens.js';
import { News } from '../collection/news.js';
import { Documents } from '../collection/documents.js';
import { Cities } from '../collection/cities.js';
import { Events } from '../collection/events.js';
import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { Poi } from '../collection/poi.js';
import { Comments } from '../collection/comments.js';
import { Lists } from '../collection/lists.js';
// DDA
import { Actions } from '../collection/actions.js';
import { Resolutions } from '../collection/resolutions.js';
import { Rooms } from '../collection/rooms.js';
import { Proposals } from '../collection/proposals.js';
// Game
import { Gamesmobile, Playersmobile, Questsmobile } from '../collection/gamemobile.js';
import { Highlight } from '../collection/highlight.js';

import { nameToCollection, arrayParent, arrayLinkParent, arrayChildrenParent, queryOrPrivateScope, getBoundingBox, queryGeoWithinFilter } from '../helpers.js';

global.Events = Events;
global.Organizations = Organizations;
global.Projects = Projects;
global.Poi = Poi;
global.Citoyens = Citoyens;
global.News = News;
global.Actions = Actions;
global.Resolutions = Resolutions;
global.Rooms = Rooms;
global.Proposals = Proposals;
global.Gamesmobile = Gamesmobile;

Events.createIndex({
  geoPosition: '2dsphere',
});
Projects.createIndex({
  geoPosition: '2dsphere',
});
Poi.createIndex({
  geoPosition: '2dsphere',
});
Organizations.createIndex({
  geoPosition: '2dsphere',
});
Citoyens.createIndex({
  geoPosition: '2dsphere',
});
Cities.createIndex({
  geoShape: '2dsphere',
});

Meteor.publish('globalautocomplete', function (query) {
  check(query, {
    name: String,
    searchType: Array,
    searchBy: String,
    indexMin: Number,
    indexMax: Number,
  });

  const self = this;
  try {
    const response = HTTP.post(`${Meteor.settings.endpoint}/communecter/search/globalautocomplete`, {
      params: query,
    });
    _.each(response.data, function (item) {
      const doc = item;
      self.added('search', Random.id(), doc);
    });
    self.ready();
  } catch (error) {
    // console.log(error);
  }
});

Meteor.publish('lists', function (name) {
  if (!this.userId) {
    return null;
  }
  check(name, String);
  const lists = Lists.find({ name });
  return lists;
});

/* Meteor.publish('notificationsUser', function () {
  if (!this.userId) {
    return null;
  }

  return ActivityStream.api.isUnread(this.userId);
}); */

Meteor.publishComposite('notificationsUser', function (limit) {
  if (!this.userId) {
    return null;
  }
  check(limit, Match.Maybe(Number));
  return {
    find() {
      const query = { type: 'notifications' };
      query.userId = this.userId;
      query.isUnread = true;
      query.objectType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
      query.targetType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
      const optionsRef = {};
      optionsRef.sort = { updated: -1 };
      if (limit) {
        optionsRef.limit = limit;
      }
      return ActivityStreamReference.find(query, optionsRef);
    },
    children: [
      {
        find(item) {
          // console.log(item.notificationId);
          const options = {};
          options.fields = {};
          options.fields[`notify.id.${this.userId}.isUnread`] = 1;
          options.fields[`notify.id.${this.userId}.isUnseen`] = 1;
          options.fields['notify.displayName'] = 1;
          options.fields['notify.labelArray'] = 1;
          options.fields['notify.icon'] = 1;
          options.fields['notify.url'] = 1;
          options.fields['notify.objectType'] = 1;
          options.fields.verb = 1;
          options.fields.target = 1;
          options.fields.object = 1;
          options.fields.created = 1;
          options.fields.author = 1;
          options.fields.type = 1;
          if (typeof variable === 'string') {
            return ActivityStream.find({ _id: new Mongo.ObjectID(item.notificationId) });
          } else {
            return ActivityStream.find({ _id: item.notificationId }, options);
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('notificationsListToBeValidated', function () {
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, { fields: { pwd: 0 } });
    },
    children: [
      {
        find(citoyen) {
          return citoyen.listToBeValidated();
        },
      },
    ],
  };
});

/* Meteor.publish('notificationsCountUser', function () {
  if (!this.userId) {
    return null;
  }
  const counterUnseen = new Counter(`notifications.${this.userId}.Unseen`, ActivityStream.api.queryUnseen(this.userId));
  const counterUnread = new Counter(`notifications.${this.userId}.Unread`, ActivityStream.api.queryUnread(this.userId));

  return [
    counterUnseen,
    counterUnread,
  ];
}); */

Meteor.publish('notificationsCountUser', function () {
  if (!this.userId) {
    return null;
  }
  const queryUnseen = { type: 'notifications' };
  queryUnseen.userId = this.userId;
  queryUnseen.isUnseen = true;
  queryUnseen.objectType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
  queryUnseen.targetType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };

  const queryUnread = { type: 'notifications' };
  queryUnread.userId = this.userId;
  queryUnread.isUnread = true;
  queryUnread.objectType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
  queryUnread.targetType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };

  const counterUnseen = new Counter(`notifications.${this.userId}.Unseen`, ActivityStreamReference.find(queryUnseen, { fields: { _id: 1 } }));
  const counterUnread = new Counter(`notifications.${this.userId}.Unread`, ActivityStreamReference.find(queryUnread, { fields: { _id: 1 } }));

  return [
    counterUnseen,
    counterUnread,
  ];
});

Meteor.publish('notificationsScopeCount', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  if (!collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isAdmin(this.userId)) {
    return null;
  }

  const queryUnseen = { type: 'notifications' };
  queryUnseen.userId = this.userId;
  queryUnseen.isUnseen = true;
  queryUnseen.targetId = scopeId;

  const queryUnread = { type: 'notifications' };
  queryUnread.userId = this.userId;
  queryUnread.isUnread = true;
  queryUnread.targetId = scopeId;

  const queryUnseenAsk = { ...queryUnseen };
  queryUnseenAsk.verb = { $in: ['ask'] };

  const counterUnseen = new Counter(`notifications.${scopeId}.Unseen`, ActivityStreamReference.find(queryUnseen, { fields: { _id: 1 } }));
  const counterUnread = new Counter(`notifications.${scopeId}.Unread`, ActivityStreamReference.find(queryUnread, { fields: { _id: 1 } }));
  const counterUnseenAsk = new Counter(`notifications.${scopeId}.UnseenAsk`, ActivityStreamReference.find(queryUnseenAsk, { fields: { _id: 1 } }));

  return [
    counterUnseen,
    counterUnread,
    counterUnseenAsk,
  ];
});

/* Meteor.publish('notificationsScope', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  if (!collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isAdmin(this.userId)) {
    return null;
  }

  const counterUnseen = new Counter(`notifications.${scopeId}.Unseen`, ActivityStream.api.queryUnseen(this.userId, scopeId));
  const counterUnread = new Counter(`notifications.${scopeId}.Unread`, ActivityStream.api.queryUnread(this.userId, scopeId));
  const counterUnseenAsk = new Counter(`notifications.${scopeId}.UnseenAsk`, ActivityStream.api.queryUnseenAsk(this.userId, scopeId));

  return [
    counterUnseen,
    counterUnread,
    counterUnseenAsk,
    collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).listNotifications(this.userId),
  ];
}); */

Meteor.publishComposite('notificationsScope', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  if (!collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isAdmin(this.userId)) {
    return null;
  }

  return {
    find() {
      const query = { type: 'notifications' };
      query.userId = this.userId;
      query.isUnseen = true;
      query.targetId = scopeId;
      query.objectType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
      query.targetType = { $nin: ['actions', 'rooms', 'cms', 'badges', 'forms'] };
      const optionsRef = {};
      optionsRef.sort = { updated: -1 };
      const limit = 100;
      if (limit) {
        optionsRef.limit = limit;
      }
      return ActivityStreamReference.find(query, optionsRef);
    },
    children: [
      {
        find(item) {
          if (typeof variable === 'string') {
            return ActivityStream.find({ _id: new Mongo.ObjectID(item.notificationId) });
          } else {
            return ActivityStream.find({ _id: item.notificationId });
          }
        },
      },
    ],
  };
});

Meteor.publish('getcitiesbylatlng', function (latlng) {
  check(latlng, { latitude: Number, longitude: Number });
  if (!this.userId) {
    return null;
  }

  return Cities.find({
    geoShape:
    {
      $geoIntersects:
      {
        $geometry: {
          type: 'Point',
          coordinates: [latlng.longitude, latlng.latitude],
        },
      },
    },
  });
});

Meteor.publish('cities', function (cp, country) {
  if (!this.userId) {
    return null;
  }
  check(cp, String);
  check(country, String);
  const lists = Cities.find({ 'postalCodes.postalCode': cp, country });
  return lists;
});

Meteor.publish('organizerEvents', function (organizerType) {
  if (!this.userId) {
    return null;
  }
  check(organizerType, String);
  check(organizerType, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'citoyens'], name);
  }));
  if (organizerType === 'organizations') {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }).listOrganizationsCreator();
  } else if (organizerType === 'projects') {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }).listProjectsCreator();
  } else if (organizerType === 'citoyens') {
    return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, { fields: { _id: 1, name: 1 } });
  }
});

Meteor.publish('citoyen', function () {
  if (!this.userId) {
    return null;
  }
  const objectId = new Mongo.ObjectID(this.userId);
  const citoyen = Citoyens.find({ _id: objectId }, { fields: { pwd: 0 } });
  return citoyen;
});

Meteor.publish('geo.dashboard', function (geoId, latlng, radius) {
  if (!this.userId) {
    return null;
  }
  check(geoId, String);
  check(latlng, Object);
  let query = {};
  if (radius) {
    check(radius, Number);
    check(latlng.longitude, Number);
    check(latlng.latitude, Number);

    // par requete sans geo
    // query = queryGeoWithinFilter(
    //   query,
    //   radius,
    //   [latlng.longitude, latlng.latitude],
    // );

    // par geoIntersects
    const nearObj = getBoundingBox(radius, [latlng.longitude, latlng.latitude]);
    const coordinates = [];
    coordinates.push([nearObj.xMin, nearObj.yMin]);
    coordinates.push([nearObj.xMax, nearObj.yMin]);
    coordinates.push([nearObj.xMax, nearObj.yMax]);
    coordinates.push([nearObj.xMin, nearObj.yMax]);
    coordinates.push([nearObj.xMin, nearObj.yMin]);
    query.geoPosition = {
      $geoIntersects: {
        $geometry: {
          type: 'Polygon',
          coordinates: [coordinates],
        },
      },
    };

    // par nearSphere
    // query.geoPosition = {
    //   $nearSphere: {
    //     $geometry: {
    //       type: 'Point',
    //       coordinates: [latlng.longitude, latlng.latitude],
    //     },
    //     $maxDistance: radius,
    //   },
    // };
  } else {
    check(latlng.type, String);
    check(latlng.coordinates, Array);
    query.geoPosition = {
      $geoIntersects: {
        $geometry: {
          type: latlng.type,
          coordinates: latlng.coordinates,
        },
      },
    };
  }
  // console.log(geoId);

  const counterEvents = new Counter(`countScopeGeo.${geoId}.events`, Events.find(query));
  const counterOrganizations = new Counter(`countScopeGeo.${geoId}.organizations`, Organizations.find(query));
  const counterProjects = new Counter(`countScopeGeo.${geoId}.projects`, Projects.find(query));
  const counterPoi = new Counter(`countScopeGeo.${geoId}.poi`, Poi.find(query));

  return [
    counterEvents,
    counterOrganizations,
    counterProjects,
    counterPoi,
  ];
});

// Geo scope
// scope string collection
// latlng object
// radius string
Meteor.publishComposite('geo.scope', function (scope, latlng, radius) {
  // check(latlng, Object);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'poi', 'organizations', 'citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const options = {};
      options._disableOplog = true;
      if (scope === 'citoyens') {
        // options.fields = { pwd: 0 };
      }
      options.fields = {
        _id: 1,
        profilThumbImageUrl: 1,
        profilMarkerImageUrl: 1,
        type: 1,
        startDate: 1,
        endDate: 1,
        geo: 1,
        name: 1,
        parent: 1,
        organizer: 1,
        organizerType: 1,
        organizerId: 1,
        parentType: 1,
        parentId: 1,
        creator: 1,
        geoPosition: 1,
        'address.codeInsee': 1,
        tags: 1,
        costum: -1,
      };

      let query = {};
      if (radius) {
        // query = queryGeoWithinFilter(
        //   query,
        //   radius,
        //   [latlng.longitude, latlng.latitude],
        // );

        query.geoPosition = {
          $nearSphere: {
            $geometry: {
              type: 'Point',
              coordinates: [latlng.longitude, latlng.latitude],
            },
            $maxDistance: radius,
          },
        };
      } else {
        query.geoPosition = {
          $geoIntersects: {
            $geometry: {
              type: latlng.type,
              coordinates: latlng.coordinates,
            },
          },
        };
      }
      if (scope === 'citoyens') {
        query._id = { $ne: new Mongo.ObjectID(this.userId) };
      }
      if (scope === 'events') {
        query.endDate = { $gte: new Date() };
      }

      // Counts.publish(this, `countScopeGeo.${scope}`, collection.find(query), { noReady: true });
      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'events') {
            return scopeD.listEventTypes();
          } else if (scope === 'organizations') {
            return scopeD.listOrganisationTypes();
          }
        },
      },
      ...arrayChildrenParent(scope, ['events', 'projects', 'organizations', 'citoyens', 'poi']),
    ],
  };
});

Meteor.publishComposite('scopeDetail', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'poi', 'organizations', 'citoyens', 'actions', 'rooms', 'proposals', 'resolutions'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      //
      if (scope === 'events') {
        // Counts.publish(this, `countSous.${scopeId}`, Events.find({ parentId: scopeId }), { noReady: true });
      }

      let query = {};

      if (_.contains(['events', 'projects', 'organizations'], scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId), 'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId), 'preferences.private': { $exists: false },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }
      // console.log(query);
      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'events') {
            return scopeD.listEventTypes();
          } else if (scope === 'organizations') {
            return scopeD.listOrganisationTypes();
          }
        },
      },
      {
        find(scopeD) {
          return Citoyens.find({
            _id: new Mongo.ObjectID(scopeD.creator),
          }, {
            fields: {
              name: 1,
              profilThumbImageUrl: 1,
            },
          });
        },
      },
      {
        find(scopeD) {
          if (scope !== 'citoyens') {
            return Citoyens.find({
              _id: new Mongo.ObjectID(this.userId),
            }, {
              fields: {
                name: 1,
                links: 1,
                collections: 1,
                profilThumbImageUrl: 1,
              },
            });
          }
        },
      },
      ...arrayChildrenParent(scope, ['events', 'projects', 'organizations', 'citoyens', 'poi']),
      {
        find(scopeD) {
          if (scopeD && scopeD.address && scopeD.address.postalCode) {
            return Cities.find({
              'postalCodes.postalCode': scopeD.address.postalCode,
            });
          }
        },
      },
    ],
  };
});

Meteor.publish('citoyenActusListCounter', function (geoId, latlng, radius) {
  if (!this.userId) {
    return null;
  }
  return new Counter(`countActus.${this.userId}`, Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }).newsActus(this.userId));
});

Meteor.publishComposite('citoyenActusList', function (limit) {
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      options.fields = {
        pwd: 0,
      };
      return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scopeD && scopeD.address && scopeD.address.postalCode) {
            return Cities.find({
              'postalCodes.postalCode': scopeD.address.postalCode,
            });
          }
        },
      },
      {
        find(scopeD) {
          // Counts.publish(this, `countActus.${this.userId}`, scopeD.newsActus(this.userId), { noReady: true });
          return scopeD.newsActus(this.userId, limit);
        },
        children: [
          {
            find(news) {
              /* ////console.log(news.author); */
              return Citoyens.find({
                _id: new Mongo.ObjectID(news.author),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
          {
            find(news) {
              return news.photoNewsAlbums();
            },
          },
          {
            find(news) {
              const queryOptions = {
                fields: {
                  _id: 1,
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              };
              if (news.target && news.target.type && news.target.id) {
                const collection = nameToCollection(news.target.type);
                return collection.find({ _id: new Mongo.ObjectID(news.target.id) }, queryOptions);
              }
            },
          },
          {
            find(news) {
              const queryOptions = {};
              if (news.object && news.object.type === 'actions') {
                queryOptions.fields = {
                  _id: 1,
                  name: 1,
                  idParentRoom: 1,
                  profilThumbImageUrl: 1,
                };
              } else if (news.object && news.object.type === 'proposals') {
                queryOptions.fields = {
                  _id: 1,
                  name: 1,
                  idParentRoom: 1,
                  profilThumbImageUrl: 1,
                };
              } else {
                queryOptions.fields = {
                  _id: 1,
                  name: 1,
                  profilThumbImageUrl: 1,
                };
              }
              if (news.object && news.object.type && news.object.id) {
                // console.log(news.object.type);
                const collection = nameToCollection(news.object.type);
                if (collection) {
                  return collection.find({ _id: new Mongo.ObjectID(news.object.id) }, queryOptions);
                }
              }
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('collectionsList', function (scope, scopeId, type) {
  check(scopeId, String);
  check(type, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['eventTypes', 'organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          return scopeD.listCollections(type, 'citoyens');
        },
      },
      {
        find(scopeD) {
          return scopeD.listCollections(type, 'organizations');
        },
      },
      {
        find(scopeD) {
          return scopeD.listCollections(type, 'projects');
        },
      },
      {
        find(scopeD) {
          return scopeD.listCollections(type, 'events');
        },
      },
      {
        find(scopeD) {
          return scopeD.listCollections(type, 'poi');
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryList', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        // options.fields = { pwd: 0 };
      }

      options.fields = {
        _id: 1,
        profilThumbImageUrl: 1,
        type: 1,
        name: 1,
        parent: 1,
        organizer: 1,
        preferences: 1,
        creator: 1,
        tags: 1,
        links: 1,
      };

      if (scope === 'events') {
        // Counts.publish(this, `countSous.${scopeId}`, Events.find({parentId:scopeId}), { noReady: true });
      }
      let query = {};

      if (_.contains(['events', 'projects', 'organizations'], scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['eventTypes', 'organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listFollowers();
          } else if (scope === 'organizations') {
            return scopeD.listFollowers();
          } else if (scope === 'projects') {
            return scopeD.listFollowers();
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listFollows();
          } else if (scope === 'organizations') {
            return scopeD.listMembers();
          } else if (scope === 'projects') {
            return scopeD.listContributors();
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listMemberOf();
          } else if (scope === 'organizations') {
            return scopeD.listMembersOrganizations();
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations') {
            return scopeD.listProjects();
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects') {
            return scopeD.listEvents();
          }
        },
      },
    ],
  };
},
);

Meteor.publishComposite('directoryListEvents', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'citoyens', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      if (scope === 'events') {
        // Counts.publish(this, `countSous.${scopeId}`, Events.find({parentId:scopeId}), { noReady: true });
      }
      let query = {};
      if (_.contains(['events', 'projects', 'organizations'], scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }
      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['eventTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listEventsCreator();
          }
        },
        children: arrayChildrenParent(scope, ['citoyens', 'organizations', 'projects', 'events']),
      },
    ],
  };
});

Meteor.publishComposite('highlight', function (localityId) {
  check(localityId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const inputDate = new Date();
      const query = {};
      query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDate };
      query.localityId = localityId;
      return Highlight.find(query);
    },
    children: [
      {
        find(scopeD) {
          if (scopeD.parentType && scopeD.parentId && _.contains(['projects', 'organizations', 'events', 'gamesmobile'], scopeD.parentType)) {
            const collectionType = nameToCollection(scopeD.parentType);
            return collectionType.find({
              _id: new Mongo.ObjectID(scopeD.parentId),
            }, {
              fields: {
                links: 0,
              },
            });
          }
        },
      },
      {
        find(scopeD) {
          if (scopeD.parentType === 'events') {
            return Lists.find({ name: { $in: ['eventTypes'] } });
          } else if (scopeD.parentType === 'organizations') {
            return Lists.find({ name: { $in: ['organisationTypes'] } });
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryListGames', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return collection.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'events') {
            return scopeD.listGamesCreator();
          }
        },
        children: arrayChildrenParent(scope, ['events']),
      },
    ],
  };
});

Meteor.publishComposite('directoryListProjects', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['organizations', 'citoyens', 'projects'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      let query = {};
      if (_.contains(['projects', 'organizations'], scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }
      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects') {
            // console.log('directoryListProjects');
            return scopeD.listProjectsCreator();
          }
        },
        children: arrayChildrenParent(scope, ['organizations', 'projects']),
      },
    ],
  };
});

Meteor.publishComposite('directoryListPoi', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'citoyens', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listPoiCreator();
          }
        },
        children: arrayChildrenParent(scope, ['citoyens', 'organizations', 'projects', 'events']),
      },
    ],
  };
});

Meteor.publishComposite('directoryListRooms', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            const options = {};
            options.fields = {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            };
            let scopeCible = scope;
            if (scope === 'organizations') {
              scopeCible = 'memberOf';
            }
            options.fields[`links.${scopeCible}`] = 1;
            return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, options);
          }
        },
        children: [
          {
            find(citoyen, scopeD) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return scopeD.listRooms();
              }
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('directoryListOrganizations', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryListInvitations', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['citoyens'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (scope === 'citoyens') {
        options.fields = { pwd: 0 };
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          }
        },
      },
      {
        find(citoyen) {
          return citoyen.listFollows();
        },
      },
    ],
  };
});

Meteor.publish('listeventSous', function (scopeId) {
  check(scopeId, String);
  if (!this.userId) {
    return null;
  }
  const query = {};
  query[`parent.${scopeId}`] = {
    $exists: false,
  };
  const counterEvents = new Counter(`countSous.${scopeId}`, Events.find(query));

  return [
    counterEvents,
    Events.find(query),
  ];
});

Meteor.publishComposite('detailRooms', function (scope, scopeId, roomId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            const options = {};
            options.fields = {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            };
            let scopeCible = scope;
            if (scope === 'organizations') {
              scopeCible = 'memberOf';
            }
            options.fields[`links.${scopeCible}`] = 1;
            return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, options);
          }
        },
        children: [
          {
            find(citoyen, scopeD) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return scopeD.detailRooms(roomId);
              }
            },
            children: [
              {
                find(room) {
                  if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                    return room.listProposals();
                  }
                },
              },
              {
                find(room) {
                  if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                    return room.listActions();
                  }
                },
              },
              {
                find(room) {
                  if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                    return room.listResolutions();
                  }
                },
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('detailProposals', function (scope, scopeId, roomId, proposalId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(proposalId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find(room) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return Proposals.find({ _id: new Mongo.ObjectID(proposalId) });
              }
            },
            children: [
              {
                find(proposal) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(proposal.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('proposalsDetailComments', function (scope, scopeId, roomId, proposalId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(proposalId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find(room) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return Proposals.find({ _id: new Mongo.ObjectID(proposalId) });
              }
            },
            children: [
              {
                find(proposal) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(proposal.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
              {
                find(proposal) {
                  return proposal.listComments();
                },
                children: [
                  {
                    find(comment) {
                      return Citoyens.find({
                        _id: new Mongo.ObjectID(comment.author),
                      }, {
                        fields: {
                          name: 1,
                          profilThumbImageUrl: 1,
                        },
                      });
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('detailActions', function (scope, scopeId, roomId, actionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(actionId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find(room) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return Actions.find({ _id: new Mongo.ObjectID(actionId) });
              }
            },
            children: [
              {
                find(action) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(action.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
              {
                find(action) {
                  return action.listMembersToBeValidated();
                },
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('actionsDetailComments', function (scope, scopeId, roomId, actionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(actionId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find(room) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return Actions.find({ _id: new Mongo.ObjectID(actionId) });
              }
            },
            children: [
              {
                find(action) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(action.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
              {
                find(action) {
                  return action.listComments();
                },
                children: [
                  {
                    find(comment) {
                      return Citoyens.find({
                        _id: new Mongo.ObjectID(comment.author),
                      }, {
                        fields: {
                          name: 1,
                          profilThumbImageUrl: 1,
                        },
                      });
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('detailResolutions', function (scope, scopeId, roomId, resolutionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(resolutionId, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['projects', 'organizations', 'events'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find(scopeD) {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
                return Resolutions.find({ _id: new Mongo.ObjectID(resolutionId) });
              }
            },
            children: [
              {
                find(resolution) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(resolution.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('detailGames', function (gameId) {
  check(gameId, String);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return Gamesmobile.find({ _id: new Mongo.ObjectID(gameId) }, options);
    },
    children: [
      {
        find(game) {
          const query = {};
          query.idGame = game._id._str;
          query.idUser = this.userId;
          const queryOptions = {};
          queryOptions.fields = {};
          queryOptions.fields.idGame = 1;
          queryOptions.fields.idUser = 1;
          queryOptions.fields.totalPoint = 1;
          queryOptions.fields.validateQuest = 1;
          queryOptions.fields.errorQuest = 1;
          queryOptions.fields.createdAt = 1;
          // queryOptions
          return Playersmobile.find(query);
        },
        children: [
          {
            find(player) {
              return player.listQuestsNoValid();
            },
          },
          {
            find(player) {
              return player.listQuestsValid();
            },
            children: [
              {
                find(quest) {
                  if (quest.questType && quest.questId) {
                    const collection = nameToCollection(quest.questType);
                    return collection.find({ _id: new Mongo.ObjectID(quest.questId) });
                  }
                },
              },
            ],
          },
          {
            find(player) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(player.idUser),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('gameScoreBoard', function (gameId) {
  check(gameId, String);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return Gamesmobile.find({ _id: new Mongo.ObjectID(gameId) }, options);
    },
    children: [
      {
        find(game) {
          return game.listPlayers();
        },
        children: [
          {
            find(player) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(player.idUser),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('listAttendees', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Events.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find(scopeD) {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(event) {
          return event.listAttendeesValidate();
        },
      },
      {
        find(event) {
          return event.listAttendeesIsInviting();
        },
      },
      {
        find(event) {
          return event.listAttendeesOrgaValidate();
        },
      },
    ],
  };
});

Meteor.publishComposite('listMembers', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Organizations.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembers();
        },
      },
    ],
  };
});

Meteor.publishComposite('listMembersToBeValidated', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['organizations', 'projects'], name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return collection.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembersToBeValidated();
        },
      },
    ],
  };
});

Meteor.publishComposite('listContributors', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Projects.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find(project) {
          return project.listContributors();
        },
      },
    ],
  };
});

Meteor.publishComposite('listFollows', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Citoyens.find({ _id: new Mongo.ObjectID(scopeId) }, {
        fields: {
          _id: 1,
          name: 1,
          'links.follows': 1,
          profilThumbImageUrl: 1,
        },
      });
    },
    children: [
      {
        find(citoyen) {
          return citoyen.listFollows();
        },
      },
    ],
  };
});

Meteor.publish('newsListCounter', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  if (!this.userId) {
    return null;
  }
  const collection = nameToCollection(scope);
  return new Counter(`countNews.${scopeId}`, collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).newsJournal(scopeId, this.userId));
});

Meteor.publishComposite('newsList', function (scope, scopeId, limit) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const collection = nameToCollection(scope);
      // Counts.publish(this, `countNews.${scopeId}`, collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).newsJournal(scopeId, this.userId), { noReady: true });
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).newsJournal(scopeId, this.userId, limit);
    },
    children: [
      {
        find(news) {
          /* ////console.log(news.author); */
          return Citoyens.find({
            _id: new Mongo.ObjectID(news.author),
          }, {
            fields: {
              name: 1,
              profilThumbImageUrl: 1,
            },
          });
        },
      },
      {
        find(news) {
          return news.photoNewsAlbums();
        },
      },
      {
        find(news) {
          const queryOptions = {
            fields: {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            },
          };
          if (news.target && news.target.type && news.target.id) {
            const collection = nameToCollection(news.target.type);
            return collection.find({ _id: new Mongo.ObjectID(news.target.id) }, queryOptions);
          }
        },
      },
      {
        find(news) {
          const queryOptions = {};
          if (news.object && news.object.type === 'actions') {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              idParentRoom: 1,
              profilThumbImageUrl: 1,
            };
          } else if (news.object && news.object.type === 'proposals') {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              idParentRoom: 1,
              profilThumbImageUrl: 1,
            };
          } else {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            };
          }
          if (news.object && news.object.type && news.object.id && news.object.type !== 'cms') {
            const collection = nameToCollection(news.object.type);
            if (collection) {
              return collection.find({ _id: new Mongo.ObjectID(news.object.id) }, queryOptions);
            }
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('newsDetail', function (scope, scopeId, newsId) {
  check(newsId, String);
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const query = {};
      if (scope === 'citoyens') {
        if (this.userId === scopeId) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'projects') {
        const collection = nameToCollection(scope);
        if (collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isContributors(this.userId)) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'organizations') {
        const collection = nameToCollection(scope);
        if (collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isMembers(this.userId)) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'events') {
        query['scope.type'] = { $in: ['restricted', 'public'] };
      }
      query._id = new Mongo.ObjectID(newsId);
      // console.log(query);
      // console.log(News.find(query).fetch())
      return News.find(query);
    },
    children: [
      {
        find(news) {
          return Citoyens.find({
            _id: new Mongo.ObjectID(news.author),
          }, {
            fields: {
              name: 1,
              profilThumbImageUrl: 1,
            },
          });
        },
      },
      {
        find(news) {
          return news.photoNewsAlbums();
        },
      },
      {
        find(news) {
          const queryOptions = {
            fields: {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            },
          };
          if (news.target && news.target.type && news.target.id) {
            const collection = nameToCollection(news.target.type);
            return collection.find({ _id: new Mongo.ObjectID(news.target.id) }, queryOptions);
          }
        },
      },
      {
        find(news) {
          const queryOptions = {};
          if (news.object && news.object.type === 'actions') {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              idParentRoom: 1,
              profilThumbImageUrl: 1,
            };
          } else if (news.object && news.object.type === 'proposals') {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              idParentRoom: 1,
              profilThumbImageUrl: 1,
            };
          } else {
            queryOptions.fields = {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            };
          }
          if (news.object && news.object.type && news.object.id && news.object.type !== 'cms') {
            const collection = nameToCollection(news.object.type);
            return collection.find({ _id: new Mongo.ObjectID(news.object.id) }, queryOptions);
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('newsDetailComments', function (scope, scopeId, newsId) {
  check(newsId, String);
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return _.contains(['events', 'projects', 'organizations', 'citoyens'], name);
  }));
  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const query = {};
      if (scope === 'citoyens') {
        if (this.userId === scopeId) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'projects') {
        const collection = nameToCollection(scope);
        if (collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isContributors(this.userId)) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'organizations') {
        const collection = nameToCollection(scope);
        if (collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isMembers(this.userId)) {
          query['scope.type'] = { $in: ['restricted', 'private', 'public'] };
        } else {
          query['scope.type'] = { $in: ['restricted', 'public'] };
        }
      } else if (scope === 'events') {
        query['scope.type'] = { $in: ['restricted', 'public'] };
      }
      query._id = new Mongo.ObjectID(newsId);
      return News.find(query);
    },
    children: [
      {
        find(news) {
          return Citoyens.find({
            _id: new Mongo.ObjectID(news.author),
          }, {
            fields: {
              name: 1,
              profilThumbImageUrl: 1,
            },
          });
        },
      },
      {
        find(news) {
          return Comments.find({
            contextId: news._id._str,
          });
        },
        children: [
          {
            find(comment) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(comment.author),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
        ],
      },
      {
        find(news) {
          return news.photoNewsAlbums();
        },
      },
    ],
  };
});
