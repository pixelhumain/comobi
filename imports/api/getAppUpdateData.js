import { Meteor } from 'meteor/meteor';
import { version } from './version.js';
import { AppUpdatesCollection } from './collection/getAppUpdateData.js';

Meteor.startup(() => {
  if (AppUpdatesCollection.find({}).count() === 0) {
    AppUpdatesCollection.insert({
      title: 'Mise à jour', message: 'Vous devez mettre à jour l\'application depuis les stores', actionLabel: '', noActionLabel: '', forceUpdate: false, ignoreVersions: ['135'],
    });
  }
});

Meteor.methods({
  getAppUpdateData({ clientVersion } = {}) {
    this.unblock();

    if (Meteor.isClient) return null;

    const appUpdate = AppUpdatesCollection.findOne() || {};

    const result = {
      ...appUpdate,
      ...(appUpdate.ignoreVersions
        && appUpdate.ignoreVersions.length
        && appUpdate.ignoreVersions.includes(clientVersion)
        ? { ignore: true }
        : {}),
      version,
    };

    // eslint-disable-next-line no-console
    console.log({
      message: `getAppUpdateData clientVersion=${clientVersion}, newClientVersion=${version}, ${JSON.stringify(
        result,
      )}`,
      appUpdateData: appUpdate,
      appUpdateResult: result,
      clientVersion,
    });
    return result;
  },
});
