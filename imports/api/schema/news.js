import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// citoyens
export const SchemasNewsRest = new SimpleSchema({
  text: {
    type: String,
    optional: true,
    custom() {
      if (!this.isSet && !this.field('media').isSet) {
        return 'required';
      }
      return null;
    },
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
  },
  tags: {
    type: Array,
    optional: true,
  },
  'tags.$': {
    type: String,
  },
  media: {
    type: Object,
    optional: true,
  },
  'media.type': {
    type: String,
    optional: true,
  },
  'media.countImages': {
    type: String,
    optional: true,
  },
  'media.images': {
    type: Array,
    optional: true,
  },
  'media.images.$': {
    type: String,
  },
  'media.content': {
    type: Object,
    optional: true,
  },
  'media.content.type': {
    type: String,
    optional: true,
  },
  'media.content.image': {
    type: String,
    optional: true,
  },
  'media.content.imageId': {
    type: String,
    optional: true,
  },
  'media.content.imageSize': {
    type: String,
    optional: true,
  },
  'media.content.videoLink': {
    type: String,
    optional: true,
  },
  'media.content.url': {
    type: String,
    optional: true,
  },
  mentions: {
    type: Array,
    optional: true,
  },
  'mentions.$': {
    type: Object,
    optional: true,
  },
  'mentions.$.id': {
    type: String,
    optional: true,
  },
  'mentions.$.name': {
    type: String,
    optional: true,
  },
  'mentions.$.avatar': {
    type: String,
    optional: true,
  },
  'mentions.$.type': {
    type: String,
    optional: true,
  },
  'mentions.$.slug': {
    type: String,
    optional: true,
  },
  'mentions.$.value': {
    type: String,
    optional: true,
  },
}, {
  tracker: Tracker,
});

export const SchemasNewsRestBase = {};

SchemasNewsRestBase.citoyens = new SimpleSchema(SchemasNewsRest, {
  tracker: Tracker,
});
SchemasNewsRestBase.citoyens.extend({
  scope: {
    type: String,
    autoValue() {
      if (this.isSet) {
        // console.log(this.value);
        return this.value;
      }
      return 'restricted';
    },
    optional: true,
  },
});

SchemasNewsRestBase.projects = new SimpleSchema(SchemasNewsRest, {
  tracker: Tracker,
});
SchemasNewsRestBase.projects.extend({
  scope: {
    type: String,
    autoValue() {
      if (this.isSet) {
        // console.log(this.value);
        return this.value;
      }
      return 'restricted';
    },
    optional: true,
  },
  targetIsAuthor: {
    type: String,
    defaultValue: 'false',
    optional: true,
  },
});

SchemasNewsRestBase.organizations = new SimpleSchema(SchemasNewsRest, {
  tracker: Tracker,
});
SchemasNewsRestBase.organizations.extend({
  scope: {
    type: String,
    autoValue() {
      if (this.isSet) {
        // console.log(this.value);
        return this.value;
      }
      return 'restricted';
    },
    optional: true,
  },
  targetIsAuthor: {
    type: String,
    defaultValue: 'false',
    optional: true,
  },
});

SchemasNewsRestBase.events = new SimpleSchema(SchemasNewsRest, {
  tracker: Tracker,
});
SchemasNewsRestBase.events.extend({
  scope: {
    type: String,
    autoValue() {
      if (this.isSet) {
        // console.log(this.value);
        return this.value;
      }
      return 'restricted';
    },
    optional: true,
  },
});
