import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema } from './schema.js';

// eslint-disable-next-line import/prefer-default-export
export const SchemasActionsRest = new SimpleSchema(baseSchema.pick('name', 'description', 'tags', 'tags.$'), {
  tracker: Tracker,
});
SchemasActionsRest.extend({
  idParentRoom: {
    type: String,
  },
  startDate: {
    type: Date,
    optional: true,
    custom() {
      if (this.field('endDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      const startDate = moment(this.value).toDate();
      const endDate = moment(this.field('endDate').value).toDate();
      if (moment(endDate).isBefore(startDate)) {
        return 'maxDateStart';
      }
    },
  },
  endDate: {
    type: Date,
    optional: true,
    custom() {
      if (this.field('startDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      const startDate = moment(this.field('startDate').value).toDate();
      const endDate = moment(this.value).toDate();
      if (moment(endDate).isBefore(startDate)) {
        return 'minDateEnd';
      }
    },
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events'],
  },
  urls: {
    type: Array,
    optional: true,
  },
  'urls.$': {
    type: String,
  },

});
