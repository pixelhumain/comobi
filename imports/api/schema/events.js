import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';
import { _ } from 'meteor/underscore';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema, blockBaseSchema, geoSchema, preferences } from './schema.js';

// collection
import { Lists } from '../collection/lists.js';

// SimpleSchema.debug = true;

export const SchemasEventsRest = new SimpleSchema(baseSchema, {
  tracker: Tracker,
});
SchemasEventsRest.extend(geoSchema);
SchemasEventsRest.extend({
  type: {
    type: String,
    autoform: {
      type: 'select',
      options() {
        if (Meteor.isClient) {
          const listSelect = Lists.findOne({
            name: 'eventTypes'
          });
          if (listSelect && listSelect.list) {
            return _.map(listSelect.list, function (value, key) {
              return {
                label: value,
                value: key
              };
            });
          }
        }
        return undefined;
      },
    },
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  email: {
    type: String,
    optional: true,
  },
  fixe: {
    type: String,
    optional: true,
  },
  mobile: {
    type: String,
    optional: true,
  },
  fax: {
    type: String,
    optional: true,
  },
  organizerId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  organizerType: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentId: {
    type: String,
    optional: true,
    autoform: {
      type: 'select',
    },
  },
  public: {
    type: Boolean,
    defaultValue: true,
  },
});

export const BlockEventsRest = {};

// BlockEventsRest.descriptions = new SimpleSchema([blockBaseSchema, baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$)]');
BlockEventsRest.descriptions = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.descriptions.extend(baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$'));

// BlockEventsRest.info = new SimpleSchema([blockBaseSchema, baseSchema.pick('name', 'url']), SchemasEventsRest.pick(['type)]');
BlockEventsRest.info = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.info.extend(baseSchema.pick('name', 'url'));
BlockEventsRest.info.extend(SchemasEventsRest.pick('type'));

BlockEventsRest.network = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.network.extend({
  github: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  instagram: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  skype: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  gpplus: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  twitter: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  facebook: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
});

// BlockEventsRest.when = new SimpleSchema([blockBaseSchema, SchemasEventsRest.pick('allDay', 'startDate', 'endDate)]');
BlockEventsRest.when = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.when.extend(SchemasEventsRest.pick('startDate', 'endDate'));

// BlockEventsRest.locality = new SimpleSchema([blockBaseSchema, geoSchema]);
BlockEventsRest.locality = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.locality.extend(geoSchema);

BlockEventsRest.preferences = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockEventsRest.preferences.extend({
  preferences: {
    type: preferences,
    optional: true,
  },
});
