import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { Gamesmobile, Playersmobile, Questsmobile } from '../collection/gamemobile.js';
import { baseSchema } from './schema.js';

export const SchemasPlayersmobileRest = new SimpleSchema({
  idUser: {
    type: String,
  },
  idGame: {
    type: String,
  },
  totalPoint: {
    type: SimpleSchema.Integer,
    optional: true,
    defaultValue: 0,
  },
  validateQuest: {
    type: Array,
    optional: true,
  },
  'validateQuest.$': {
    type: String,
  },
  errorQuest: {
    type: Array,
    optional: true,
  },
  'errorQuest.$': {
    type: String,
  },
  finishedAt: {
    type: Date,
    optional: true,
  },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset();
    },
  },
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
});

Playersmobile.attachSchema(SchemasPlayersmobileRest, {
  tracker: Tracker,
});

/* Todo
questionType :

*/
export const SchemasQuestsmobileRest = new SimpleSchema({
  idGame: {
    type: String,
  },
  pointWin: {
    type: SimpleSchema.Integer,
    min: 1,
  },
  order: {
    type: SimpleSchema.Integer,
  },
  numberPlayerValidate: {
    type: SimpleSchema.Integer,
    optional: true,
    defaultValue: 0,
  },
  numberPlayerError: {
    type: SimpleSchema.Integer,
    optional: true,
    defaultValue: 0,
  },
  question: {
    type: String,
  },
  questType: {
    type: String,
  },
  questId: {
    type: String,
  },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset();
    },
  },
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
});

Questsmobile.attachSchema(SchemasQuestsmobileRest, {
  tracker: Tracker,
});

export const SchemasGamesmobileRest = new SimpleSchema(baseSchema.pick('name', 'description'));
SchemasGamesmobileRest.extend({
  startDate: {
    type: Date,
    optional: true,
    custom() {
      if (this.field('endDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      const startDate = moment(this.value).toDate();
      const endDate = moment(this.field('endDate').value).toDate();
      if (moment(endDate).isBefore(startDate)) {
        return 'maxDateStart';
      }
    },
  },
  endDate: {
    type: Date,
    optional: true,
    custom() {
      if (this.field('startDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      const startDate = moment(this.field('startDate').value).toDate();
      const endDate = moment(this.value).toDate();
      if (moment(endDate).isBefore(startDate)) {
        return 'minDateEnd';
      }
    },
  },
  numberPlayers: {
    type: SimpleSchema.Integer,
    defaultValue: 0,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events'],
  },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {
          $setOnInsert: new Date()
        };
      }
      this.unset();
    },
  },
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
});

Gamesmobile.attachSchema(SchemasGamesmobileRest, {
  tracker: Tracker,
});
