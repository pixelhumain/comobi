import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';
import { _ } from 'meteor/underscore';
import { Tracker } from 'meteor/tracker';

import { Lists } from '../collection/lists.js';

// schemas
import {
  baseSchema, blockBaseSchema, geoSchema, preferencesSelect,
} from './schema.js';

const baseSchemaCitoyens = baseSchema.pick('name', 'shortDescription', 'description', 'url', 'tags', 'tags.$');

const updateSchemaCitoyens = new SimpleSchema({
  /* username: {
    type: String,
    custom () {
      if (Meteor.isClient && this.isSet) {
        Meteor.call('checkUsername', this.value, function (error, result) {
          // console.log(result);
          if (!result) {
            updateSchemaCitoyens.namedContext('editBlockCitoyen').addValidationErrors([{ name: 'username', type: 'notUnique' }]);
          }
        });
      }
    },
  }, */
  email: {
    type: String,
    unique: true,
  },
  fixe: {
    type: String,
    optional: true,
  },
  mobile: {
    type: String,
    optional: true,
  },
  fax: {
    type: String,
    optional: true,
  },
  birthDate: {
    type: Date,
    optional: true,
  },
  github: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  telegram: {
    type: String,
    optional: true,
  },
  skype: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  /* gpplus: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  }, */
  twitter: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  facebook: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
});

/* export const SchemasCitoyensRest = new SimpleSchema([baseSchemaCitoyens, updateSchemaCitoyens, geoSchema, {
  preferences: {
    type: Object,
    optional: true,
  },
  'preferences.isOpenData': {
    type: Boolean,
  },
}]); */

export const SchemasCitoyensRest = new SimpleSchema(baseSchemaCitoyens, {
  tracker: Tracker,
});
SchemasCitoyensRest.extend(updateSchemaCitoyens);
SchemasCitoyensRest.extend(geoSchema);
SchemasCitoyensRest.extend(updateSchemaCitoyens);
SchemasCitoyensRest.extend({
  preferences: {
    type: Object,
    optional: true,
  },
  'preferences.isOpenData': {
    type: Boolean,
  },
});

export const BlockCitoyensRest = {};
// BlockCitoyensRest.descriptions = new SimpleSchema([blockBaseSchema, baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$)]');
BlockCitoyensRest.descriptions = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockCitoyensRest.descriptions.extend(baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$'));

// BlockCitoyensRest.info = new SimpleSchema([blockBaseSchema, baseSchema.pick('name', 'url']), updateSchemaCitoyens.pick(['email', 'fixe', 'mobile', 'fax', 'birthDate)]');
BlockCitoyensRest.info = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockCitoyensRest.info.extend(baseSchema.pick('name', 'url'));
BlockCitoyensRest.info.extend(updateSchemaCitoyens.pick('email', 'fixe', 'mobile', 'fax', 'birthDate'));

// BlockCitoyensRest.network = new SimpleSchema([blockBaseSchema, updateSchemaCitoyens.pick('github', 'telegram', 'skype', 'gpplus', 'twitter', 'facebook)]');
BlockCitoyensRest.network = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockCitoyensRest.network.extend(updateSchemaCitoyens.pick('github', 'telegram', 'skype', 'twitter', 'facebook'));

// BlockCitoyensRest.locality = new SimpleSchema([blockBaseSchema, geoSchema]);
BlockCitoyensRest.locality = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockCitoyensRest.locality.extend(geoSchema);

/* BlockCitoyensRest.preferences = new SimpleSchema([blockBaseSchema, {
  preferences: {
    type: Object,
    optional: true,
  },
  'preferences.email': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.locality': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.phone': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.directory': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.birthDate': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.isOpenData': {
    type: Boolean,
    optional: true,
  },
}]); */

BlockCitoyensRest.preferences = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockCitoyensRest.preferences.extend({
  preferences: {
    type: Object,
    optional: true,
  },
  'preferences.email': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.locality': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.phone': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.directory': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.birthDate': {
    type: String,
    allowedValues: preferencesSelect,
    optional: true,
  },
  'preferences.isOpenData': {
    type: Boolean,
    optional: true,
  },
});

// type : person / follow
// invitedUserName
// invitedUserEmail
export const SchemasFollowRest = new SimpleSchema({
  invitedUserName: {
    type: String,
  },
  invitedUserEmail: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
}, {
  tracker: Tracker,
});

export const SchemasInviteAttendeesEventRest = new SimpleSchema({
  invitedUserName: {
    type: String,
  },
  invitedUserEmail: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
  eventId: {
    type: String,
  },
}, {
  tracker: Tracker,
});

export const SchemasInvitationsRest = new SimpleSchema({
  childId: {
    type: String,
    optional: true,
  },
  childName: {
    type: String,
  },
  childEmail: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
  childType: {
    type: String,
  },
  organizationType: {
    type: String,
    optional: true,
    autoform: {
      type: 'select',
      options() {
        if (Meteor.isClient) {
          const listSelect = Lists.findOne({ name: 'organisationTypes' });
          if (listSelect && listSelect.list) {
            return _.map(listSelect.list, function (value, key) {
              return { label: value, value: key };
            });
          }
        }
        return undefined;
      },
    },
  },
  parentType: {
    type: String,
  },
  parentId: {
    type: String,
  },
  connectType: {
    type: String,
    optional: true,
  },
}, {
  tracker: Tracker,
});

export const SchemasReportFlagRest = new SimpleSchema({
  reportReason: {
    type: String,
  },
  reportMessage: {
    type: String,
  },
  cibleScope: {
    type: String,
  },
  cibleId: {
    type: String,
  },
}, {
  tracker: Tracker,
});

export const SchemasDeleteMyAccountRest = new SimpleSchema({
  reason: {
    type: String,
    optional: true,
  },
}, {
  tracker: Tracker,
});

export const SchemasChangePasswordRest = new SimpleSchema({
  oldPassword: {
    type: String,
  },
  newPassword: {
    type: String,
  },
}, {
  tracker: Tracker,
});
