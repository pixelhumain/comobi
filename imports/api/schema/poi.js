import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema, blockBaseSchema, geoSchema } from './schema.js';

// SimpleSchema.debug = true;

export const SchemasPoiRest = new SimpleSchema(baseSchema, {
  tracker: Tracker,
});
SchemasPoiRest.extend(geoSchema);
SchemasPoiRest.extend({
  type: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentType: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  urls: {
    type: Array,
    optional: true,
  },
  'urls.$': {
    type: String,
  },
});

export const BlockPoiRest = {};

BlockPoiRest.descriptions = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockPoiRest.descriptions.extend(baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$'));

BlockPoiRest.info = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockPoiRest.info.extend(baseSchema.pick('name'));

BlockPoiRest.locality = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockPoiRest.locality.extend(geoSchema);
