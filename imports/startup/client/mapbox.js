import { Meteor } from 'meteor/meteor';
import { Mapbox } from 'meteor/communecter:mapbox';
import { Maptiler } from 'meteor/communecter:maptiler';

Meteor.startup(function () {
  Mapbox.load({
    plugins: ['markercluster'],
  });
  Maptiler.load();
});
