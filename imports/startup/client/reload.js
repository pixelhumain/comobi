/* eslint-disable no-console */
/* eslint-disable meteor/no-session */
/* global Session IonPopup device */
import { Meteor } from 'meteor/meteor';
import { Reloader } from 'meteor/quave:reloader';
import { Tracker } from 'meteor/tracker';

import { methodCall } from '../../infra/methodCall.js';
import { version } from '../../api/version.js';
import { pageSession } from '../../api/client/reactive.js';

Meteor.startup(async () => {
  Session.set('updateStore', false);
  if (Meteor.isCordova) {
    try {
      const appUpdateData = (await methodCall('getAppUpdateData', { clientVersion: version })) || {};
      pageSession.set('appUpdateData', appUpdateData);
      if (appUpdateData.version !== version && appUpdateData.forceUpdate && !appUpdateData.ignore) {
        Session.set('updateStore', true);
      }
    } catch (e) {
      console.info({
        message: 'forcing app reload because getAppUpdateData is breaking',
      });
      return;
    }
  }
});

if (Meteor.isDevelopment) {
  console.log('reload dev');
  Reloader.initialize({
    beforeReload(ok, nok) {
      if (device.platform === 'Android') {
        IonPopup.alert({
          template: 'Fermez et réouvrez l\'application pour obtenir la nouvelle version!',
        });
      }
      nok();
    },
  });
} else if (Meteor.isCordova) {
  Reloader.initialize({
    beforeReload(ok, nok) {
      if (device.platform === 'Android') {
        IonPopup.alert({
          template: 'Fermez et réouvrez l\'application pour obtenir la nouvelle version!',
        });
      }
      nok();
    },
  });
}
