/* eslint-disable no-console */
/* eslint-disable meteor/no-session */
/* global Session IonPopup device */
import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    if (Meteor.isCordova) {
        if (Meteor.settings.public.updatePlugin) {
            window.plugins.updatePlugin.update(() => {
                // success callback
            }, () => {
                // error callback
            }, Meteor.settings.public.updatePlugin);
        }
    }
});
