import '../../../i18n/en.i18n.json';
import '../../../i18n/fr.i18n.json';
import '../../api/simpleschema-messages.js';
import '../../api/helper';
import './startup.js';
import './observePush.js';
import '../../infra/rest.js';
import '../../infra/meta-tags.js';
